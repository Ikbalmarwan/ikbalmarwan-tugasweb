<?php

namespace App\Http\Controllers;

use App\Models\MhsNim;
use Illuminate\Http\Request;

class MhsNimController extends Controller
{
    public function index()
    {
        $mhs = MhsNim::all();
        return view('mhs.index', compact('mhs'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'nim' => 'required',
            'nama' => 'required',
        ]);

        MhsNim::create($data);

        return redirect('/mhs');
    }
}
